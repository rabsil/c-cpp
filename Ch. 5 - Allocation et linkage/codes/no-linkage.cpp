#include <iostream>

#include "no-linkage.h"

using namespace std;

void g()
{
    struct A //A has no linkage
    {
        int k; //k has no linkage, because A has no linkage
    }; 
    
    using byte_ptr = unsigned char*; //byte_ptr has no linkage
}
    

int main()
{
    if(true)
    {
        auto f = [](int i) //f and i have no linkage
        {                        
            int j = i * 2; //j has no linkage
            
            cout << i << " " << j << endl;  
        };
        
        //cout << i << endl;
        //cout << j << endl;
        
        g();
        
        //A a;
        //decltype(A::k) l = 0;
        
        //byte_ptr b = 0;
    }
}
