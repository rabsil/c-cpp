#ifndef INLINE_H
#define INLINE_H

#include <iostream>

struct A
{
    void f() //inline
    {
        std::cout << "Brol::f" << std::endl;
    }
};

struct B
{
    inline void f();//inline
};

void B::f()
{
    std::cout << "Foo::f" << std::endl;//defined in same file
}

double diff(double a, double b); //not inline

inline double sum(double a, double b) { return a + b; } //inline

struct C
{
    void f();//not inline
};

#endif