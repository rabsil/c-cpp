#ifndef STATIC_ATT
#define STATIC_ATT

class A
{
    static int count;
    int _id;
    
    public:
        A();
    
        int id() const;
};

#endif