#include "inline.h"

#include <iostream>

void C::f()
{
    std::cout << "C::f" << std::endl;
}

double diff(double a, double b)
{
    return a - b;   
}