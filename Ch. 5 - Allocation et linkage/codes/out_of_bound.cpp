#include <iostream>

using namespace std;

int main()
{
    int a = 4;
    int t[] = {0, 1, 2};
    int b = 12;
    
    cout << &a << " " << t << " " << &b << endl;
    
    t[-1] = 42;
    
    cout << a << " " << b << endl;
}
