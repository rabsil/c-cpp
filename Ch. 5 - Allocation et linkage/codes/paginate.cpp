#include <iostream>
#include <thread>
#include <cstdint>

using namespace std;

void mishmash(int32_t* p, int size)
{
    int dummy;
    for(int i = 0; i < size; i++)
    {
        *p = 42;
        dummy = *p;
        p++;
    }
}

int main()
{		        
	long long unsigned j = 0;
	while(true)
	{
        static long long unsigned m10MB = 250 * 1000 * 10;
		int32_t * pt = new int32_t[m10MB]; //10MB
        mishmash(pt, m10MB);
        static long long unsigned int rounds2GB = 200; //loop rounds needed to allocate 2GB
		j++;
		if(j % 10 == 0) //100MB per 100MB
		{
			cout << (j*10) << "MB allocated" << endl;
			this_thread::sleep_for(100ms);
		}
		if(j >= rounds2GB)
			break;
	}

	cout << "Type sth to close" << endl;
	cin >> j; //blocking
}	
