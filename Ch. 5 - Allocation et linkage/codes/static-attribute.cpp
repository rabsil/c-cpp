#include "static-attribute.h"

int A::count = 1;

A::A() : _id(count)
{
    count++;
}

int A::id() const
{
    return _id;   
}