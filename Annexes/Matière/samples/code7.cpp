#include <iostream>
#include <vector>

using namespace std;

int min(const auto& iterable)
{
    int min;
    for(const auto& e : iterable)
        if(e < min)
            min = e;
    return min;
}

int main()
{
    vector<int> v = {4, 3, 1, 8, 5, 9, 10};
    cout << min(v) << endl;
}

//want 1
//have -42
