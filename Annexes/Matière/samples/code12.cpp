#include <iostream>

using namespace std;

struct Token
{
    int data;
    Token next;
};

int main()
{
    Token t1 {2}; Token t2 {3};
    t1.next = t2;
    cout << t1.data << " " << t2.data << endl;
}

//want 2 3
//have comp. error
