#include <iostream>

using namespace std;

struct couple
{
    int x, y;
    bool operator==(const couple&) const = default;
    auto operator<=>(const couple&) const = default;
};

int main()
{
    couple c1 = {2,1};
    couple c2 = {1,2};
    cout << (c1 < c2) << endl;
}

//want 1
//has 0
