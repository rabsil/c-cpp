#include "code22.h"

bool is_even(int n)
{
    return n % 2 == 0;
}