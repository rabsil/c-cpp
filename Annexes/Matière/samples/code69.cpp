#include <iostream>

using namespace std;

template<int N>
struct A
{  
    A() { cout << N << endl; }
};

int main()
{
    int i = 2;
    A<i> a;
}

//want 2
//has comp. error