#include <iostream>

using namespace std;

int main()
{
    int i = 3;
    auto f = [=]()
    {
        i++;
    };
    f();
    cout << i << endl;
}

//want 4
//have comp. error