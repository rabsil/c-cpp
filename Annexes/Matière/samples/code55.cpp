#include <iostream>

using namespace std;

class Array
{
    int* a;
    unsigned s;
    public:
        Array(unsigned s) : a(new int[s]), s(s) {}
        ~Array() { delete[] a; }
        int& operator[](unsigned k) { return a[k]; }
};

void f(Array a) {}

int main()
{
    Array a(5);
    Array b(6); 
    for(unsigned i = 0; i < 6; i++)
        b[i] = i;
    a = b;
    f(b);
    for(unsigned i = 0; i < 6; i++)
        cout << a[i] << " ";
    cout << endl;
}

//want 0 1 2 3 4 5
//has seg fault
