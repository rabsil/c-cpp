#include <iostream>

using namespace std;

struct A
{
    bool operator!=(const A& a) const { return false; }  
};

int main()
{
    A a;
    cout << (a == a) << endl;
}

//want 1
//has comp. error
