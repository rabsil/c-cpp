#include <iostream>

template<class T1, class T2, int I>
struct A {}; 
 
template<class T, int I>
struct A<T, T*, I> 
{
    A() { std::cout << I << std::endl; }
};
 
template<class T1, class T2, int I>
struct A<T1*, T2, I>
{
    A() { std::cout << I << std::endl; }
};

template<class T1, class T2, int I>
struct A<T1, T2*, I>
{
    A() { std::cout << I << std::endl; }
};

int main()
{	
	A<int*, int*, 2> a; 
}

//want 2
//has comp. error