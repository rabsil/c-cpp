#include <iostream>

using namespace std;

auto factorial(auto n)
{
    return n == 0 ? 1 : n * factorial(n-1);
}

int main()
{
    cout << factorial(4) << endl;
}

//want 24
//have comp. error