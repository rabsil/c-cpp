#ifndef A_H
#define A_H

#include "code33-b.h"

struct A
{
    int _i;
    B* _b;
    
    int i() const
    {
        return _i;   
    }
};

#endif