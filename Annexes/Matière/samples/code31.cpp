#include <iostream>
#include <memory>

using namespace std;

class B;

struct A
{
    shared_ptr<B> pt;
    ~A() { cout << "-A" << endl; };
};

struct B
{
    shared_ptr<A> pt;
    ~B() { cout << "-B" << endl; };
};

int main()
{
    auto a = make_shared<A>();
    auto b = make_shared<B>();
    a->pt = b; b->pt = a;
    cout << "main" << endl;
}

//want -B -A main
//has main
