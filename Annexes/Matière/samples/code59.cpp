#include <iostream>

using namespace std;

class A 
{    
    protected:
        int i;
    public:
        A(int i) : i (i) {}
};

struct B : A
{
    void set(int i) { this-> i = i; }
    void print() { cout << i << endl; }
};

int main()
{
    B b; b.set(2);
    b.print();
}

//want 2
//has comp. error
