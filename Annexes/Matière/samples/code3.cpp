#include <iostream>

using namespace std;

void swap(int * i, int * j) 
{
    int * tmp = i;
    i = j; 
    j = tmp;
}

int main() 
{
    int i = 2; int j = 3; 
    swap(&i, &j);
    cout << i << " " << j << endl;
}

//want 3 2
//have 2 3