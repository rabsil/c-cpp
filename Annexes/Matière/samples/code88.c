#include <stdio.h>

const int* make_tab() 
{    
    const int t[] = {1,2,3,4,5};
    return t;
}

int main() 
{
    const int* t = make_tab();
    for(unsigned i = 0; i < 5; i++)
        printf("%d ", t[i]);
}

//want 1 2 3 4 5
//has seg fault