#include <iostream>

using namespace std;

auto factorial(auto n)
{
    if(n != 0)
        return n * factorial(n-1);
    else
        return 1;
}

int main()
{
    cout << factorial(4) << endl;
}

//want 24
//have comp. error
