#include <iostream>

struct A
{
    virtual void f() = 0;  
};

struct B : A
{
    void f() override { std::cout << "Hello" << std::endl; }
};

void f(A a)
{
    a.f();   
}

int main()
{
    B b;
    f(b);
}

//want Hello
//has comp. error