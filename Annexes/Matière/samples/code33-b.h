#ifndef B_H
#define B_H

#include "code33-a.h"

struct B
{
    int _i;
    A* _a;
    
    int i() const
    {
        return _i;   
    }
};

#endif