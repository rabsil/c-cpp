#include <iostream>

using namespace std;

struct A
{
    ~A() = delete;  
};

void f(A a) { cout << "f" << endl; }

int main()
{
    A a;
    f(a);
}

//want f
//has comp. error