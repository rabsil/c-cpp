#include <iostream>

struct A 
{
    void f() 
    {
        std::cout << "f" << std::endl;
    }
};

struct B 
{ 
    int i; 
    void g()
    {
        std::cout << "g" << std::endl;
    }
};

template<class T>
concept looks_like_A(T t) requires
{
    { t.f() }  
};

template<class T> bool is_A(T t)
{
    return sizeof(T) == sizeof(A);   
}

template<class T> bool is_B(T t)
{
    return sizeof(T) == sizeof(B);   
}

template<class T>
void f(T t)
{
    if (is_A<T>(t))
        t.f();
    else
        t.g();
}

int main()
{
    A a; B b;
    f(a);
    f(b);
}

//want f g
//has comp. error
