#include <iostream>

struct Duck 
{ 
    void quack() { std::cout << "Quack" << std::endl; } 
};

template<class T>
concept looks_like_duck = requires(const T& t)
{
    { t.quack() };
};

template<looks_like_duck D>
void squish(D& d)
{
    d.quack(); 
}

int main()
{
    Duck d;
    squish(d);
}

//want Quack
//has comp. error
