#include <iostream>

using namespace std;

struct Boolean
{
    bool val;
    bool log_and(bool b) 
    { 
        val &= b; 
        return val; 
    }
    
    bool log_or(bool b) 
    { 
        val |= b; 
        return val; 
    }
};

int main()
{
    Boolean b {false};
    cout << b.log_and(true) << " " << b.log_or(true) << endl;
}

//want 0 1
//have 1 1
