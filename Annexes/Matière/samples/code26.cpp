#include <iostream>

using namespace std;

void f(int i = 0, int j)
{
    cout << i << " " << j << endl;   
}

int main()
{
    f(10);
}

//want 0 10
//have comp. error