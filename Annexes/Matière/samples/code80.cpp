#include <iostream>

template<class T>
struct A
{
    T t;
    int i;
    A(int i) : i(i) { std::cout << i << std::endl; }
    A(T t) : t(t) { std::cout << t << std::endl; }
};

int main()
{
    A<int> a = 2;
}

//want 2
//has comp. error