#include <iostream>
#include "code33-a.h"
#include "code33-b.h"

int main()
{
    A a {1}; B b { 2, &a }; a._b = &b;
    std::cout << a.i() << " " << b.i() << std::endl;
}

//want 1 2
//has comp. error