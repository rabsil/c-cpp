#include <iostream>

using namespace std;

void f(void) { cout << "v" << endl; }
void f(int) { cout << "i" << endl; }
void g(void* p) { cout << "pv" << endl; }
void g(int* p) { cout << "pi" << endl; }


int main()
{
    char c = 'c';
    f(c);
    g(&c);
}

//want i pi
//have i pv
