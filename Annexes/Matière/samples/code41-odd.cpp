#include "code41-odd.h"
#include "code41-even.hpp"

bool is_odd(int n)
{
    return ! is_even(n);   
}