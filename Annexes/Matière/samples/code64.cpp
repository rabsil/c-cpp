#include <iostream>

struct A
{
    virtual A& operator=(const A& a) { std::cout << "=A" << std::endl; return *this; }  
};

struct B : A
{
    virtual B& operator=(const B& a) { std::cout << "=B" << std::endl; return *this; }  
};

int main()
{
    A * a = new A; B * b = new B;
    a = b;
    cout << "Hello" << endl;
}

//want =B Hello
//has Hello
