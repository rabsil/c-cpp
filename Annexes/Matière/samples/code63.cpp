#include <iostream>

struct A
{
    void f(int) { std::cout << "int" << std::endl; }
    void f(char) { std::cout << "char" << std::endl; }
};

struct B : A
{
    void f(int,int) { std::cout << "int,int" << std::endl; }  
};

int main()
{
    B b;
    b.f(1);
    b.f('c');
}

//want int char
//has comp. error