#include <iostream>

using namespace std;

struct A {};
struct B : A {};

void f() { throw B(); }

int main()
{
    try
    {
        f();   
    }
    catch(const A& a) { cout << "A" << endl; }
    catch(const B& a) { cout << "B" << endl; }
    catch(...) { cout << "..." << endl; }
}

//want B
//has A
