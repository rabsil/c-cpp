#include <iostream>

using namespace std;

template<class T>
const T& higher(const T& t1, const T& t2)
{
    return t1 > t2 ? t1 : t2;   
}

int main()
{
    cout << higher(1, 2.0) << endl;
}

//want 0
//has comp. error