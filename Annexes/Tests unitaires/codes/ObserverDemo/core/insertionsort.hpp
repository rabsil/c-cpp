#ifndef INSERTIONSORT_H
#define INSERTIONSORT_H

#include "array.hpp"
#include "sortalgorithm.hpp"

template<class T>
class InsertionSort : public SortAlgorithm<T>
{
    public:
        InsertionSort(Array<T>& a);
        void sort();        
};

template<class T>
InsertionSort<T>::InsertionSort(Array<T> &array) : SortAlgorithm<T>(array) {}

template<class T>
void InsertionSort<T>::sort()
{
    for (int i = 1; i < this->a.size(); i++)
    {
        T x = this->a[i];
        int j = i - 1;
        for(; j >= 0 && this->a[j] > x ; j--)
            this->a.set(j+1, this->a[j]);
        this->a.set(j + 1, x);
    }
}

#endif // INSERTIONSORT_H
