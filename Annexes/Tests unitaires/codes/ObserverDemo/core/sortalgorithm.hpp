#ifndef SORTALGORITHM_H
#define SORTALGORITHM_H

#include "array.hpp"

template<class T>
class SortAlgorithm
{
    protected:
        Array<T> & a;

    public:
        SortAlgorithm(Array<T> & array);
        inline Array<T>& array() const;
        virtual void sort() = 0;
};

template<class T>
SortAlgorithm<T>::SortAlgorithm(Array<T> &array) : a(array) {}

template<class T>
Array<T>& SortAlgorithm<T>::array() const
{
    return a;
}

#endif // SORTALGORITHM_H
