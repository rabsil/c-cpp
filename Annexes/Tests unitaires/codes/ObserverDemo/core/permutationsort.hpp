#ifndef PERMUTATIONSORT_H
#define PERMUTATIONSORT_H

#include "array.hpp"
#include "sortalgorithm.hpp"

template<class T>
class PermutationSort : public SortAlgorithm<T>
{
    public:
        PermutationSort(Array<T>& a);
        void sort();        
};

template<class T>
PermutationSort<T>::PermutationSort(Array<T> &array) : SortAlgorithm<T>(array) {}

template<class T>
void PermutationSort<T>::sort()
{    
    for(int i = 0 ; i < this->a.size() -1; i++)
    {
        int min = i;
        for(int j = i+1 ; j < this->a.size(); j++)
            if(this->a[min] > this->a[j])
                min = j;

        this->a.swap(i, min);
    }
}

#endif // PERMUTATIONSORT_H
