#ifndef BUBBLESORT_H
#define BUBBLESORT_H

#include "array.hpp"
#include "sortalgorithm.hpp"

template<class T>
class BubbleSort : public SortAlgorithm<T>
{
    public:
        BubbleSort(Array<T>& a);
        void sort();
};

template<class T>
BubbleSort<T>::BubbleSort(Array<T> &array) : SortAlgorithm<T>(array) {}

template<class T>
void BubbleSort<T>::sort()
{
    for (int i = this->a.size() - 1; i >= 0; i--)
        for (int j = 0; j < i; j++)
            if(this->a[j] > this->a[j+1])
                this->a.swap(j, j+1);
}


#endif // BUBBLESORT_H
