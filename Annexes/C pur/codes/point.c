#include "stdio.h"

struct point
{
    int x, y;
};

struct point2
{
    int x, y;  
};

struct point default_point()
{
    struct point p = {0, 0};
    return p;
}

void print(struct point p)
{
    printf("( %d , %d )\n", p.x, p.y);
}

//void print(struct point2 p) //no overload
//{
//    printf("( %d , %d )\n", p.x, p.y);
//}

int main()
{
    struct point p1;
    print(p1); //undefined values
    
    struct point p2 = {1, 2};
    print(p2);
    
    struct point2 p3;
    //print(p3);
    //print((struct point)p3);
}
