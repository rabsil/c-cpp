#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

typedef struct node
{
    struct node * next; //no alias here
    int data;
} node;

typedef struct
{
    node * head;
    node * tail;
    int size;
} LinkedList;

void make_node(node* n, int data)
{
    n->next = NULL;
    n->data = data;
}

void make_list(LinkedList* l)     
{
    l->head = NULL;
    l->tail = NULL;
    l->size = 0;
}

int list_size(LinkedList l)
{
    return l.size;
}

void print_list(LinkedList l)
{
    printf("{ ");
    for(node * current = l.head; current != NULL; current = current->next)
        printf("%d ", current->data);
    printf("}\n");
}

void list_add(LinkedList* l, int data)
{
    node * n = (node*) malloc(sizeof(node));
    make_node(n, data);
    
    if(l->head == NULL && l->tail == NULL) //empty list
    {
        l->head = n;
        l->tail = n;
    }
    else
    {
        l->tail->next = n;
        l->tail = n;
    }

    l->size = l->size + 1;
}

bool list_remove(LinkedList* l, int data)
{
    node * prev = NULL;
    for(node * current = l->head; current != NULL; current = current->next)
    {
        if(current->data == data)
        {
            prev->next = current->next;
            free(current);
            l->size--;
            return true;
        }
        prev = current;
    }

    return false;
}

void delete_list(LinkedList* l)
{
    node * next = NULL;
    node * current = l->head;

    while(current != NULL)
    {
        next = current->next;
        free(current);
        current = next;
    }
}

void copy_list(LinkedList source, LinkedList* dest)
{
    make_list(dest);
    for(node * current = source.head; current != NULL; current = current->next)
        list_add(dest, current->data);
}

int main()
{
    LinkedList l; //allocated on stack
    make_list(&l);
    printf("l : %p %p %d\n", l.head, l.tail, l.size);

    print_list(l);

    for(int i = 0; i < 5; i++)
        list_add(&l, i * 2);

    print_list(l);    

    bool r = list_remove(&l, 4);

    printf("%s\n", r ? "true" : "false");
    print_list(l);

    r = list_remove(&l, 40);    

    printf("%s\n", r ? "true" : "false");
    print_list(l);
    delete_list(&l);

    /** seg fault
    LinkedList * l2;
    make_list(l2);
    printf("l2 : %p %p %d\n", l2->head, l2->tail, l2->size);

    print_list(*l2);    
    */

    LinkedList * l3 = (LinkedList*) malloc(sizeof(LinkedList)); //allocated on heap
    make_list(l3);
    printf("l3 : %p %p %d\n", l3->head, l3->tail, l3->size);

    print_list(*l3);
    
    for(int i = 0; i < 5; i++)
        list_add(l3, i * 3);

    print_list(*l3);    
    
    LinkedList l4;
    copy_list(*l3, &l4);
    print_list(l4);
    delete_list(&l4);
    
    delete_list(l3);
    free(l3);    
    //free(l3); //most likely seg fault
}
