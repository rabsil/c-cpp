#include "stdio.h"

typedef struct point
{
    int x, y;
} point;

point default_point()
{
    struct point p = {0, 0};
    return p;
}
#define DEFAULT_POINT {0, 0}
//point DEFAULT_POINT = {0, 0}; //other option

void print(point p)
{
    printf("( %d , %d )\n", p.x, p.y);
}

int main()
{
    point p1;
    print(p1); //undefined values
    
    point p2 = DEFAULT_POINT;
    print(p2);
    
    point p3 = {1, 2};
    print(p3);    
}
