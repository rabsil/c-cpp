#include <iostream>

using namespace std;

void f(int)
{
    cout << "f(int)" << endl;
}

void f(long double)
{
    cout << "f(long double)" << endl;
}

void g(long int)
{
    cout << "g(long int)" << endl;
}

void g(double)
{
    cout << "g(long int)" << endl;
}

void h(int)
{
    cout << "h(int)" << endl;
}

void hh(long int)
{
    cout << "hh(long int)" << endl;
}

void hh(double)
{
    cout << "hh(double)" << endl;
}

int main()
{
    f(2);
    //f(2L); //ko: long -> int and long -> long double are both convs
    //f(2.); //ko: double -> int and double -> long double are both conv
    f(2.L);
    
    //g(2); //ko: int -> long int and int -> double are both convs
    g(2L);
    g(2.);
    //g(2.L); //ko: long double -> double and long double -> int are both convs
    
    short s;
    h(s);
    h(true);
    h('a');
    
    hh(s); //ko: short -> int -> long int and short -> int -> double are both convs
    hh(true); //ko: bool -> int -> long int and bool -> int -> double are both convs
    hh('a'); //ko: char -> int -> long int and char -> int -> double are both convs
}
