#include <iostream>

using namespace std;

void fct(auto i)
{ 
    cout << "fct(auto) with param=" << typeid(i).name() << endl;
}

void fct(auto& i)
{
    cout << "fct(auto&) with param=" << typeid(i).name() << endl;
}

/*
void fct(const auto& i) //try to uncomment
{
    cout << "fct(const auto&) with param=" << typeid(i).name() << endl;
}
*/

void fct(int i) //try to comment
{
    cout << "fct(int)" << endl;
}

void fct(long i)
{
    cout << "fct(long)" << endl;
}

int main()
{
    fct(2);   
    fct(2.5);
}
