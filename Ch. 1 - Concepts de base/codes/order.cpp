#include <iostream>

void g();

int main() 
{
    f();
    g();
}

void f()
{
    std::cout << "Hello ";
}

void g()
{
    std::cout << "world!" << std::endl;
}