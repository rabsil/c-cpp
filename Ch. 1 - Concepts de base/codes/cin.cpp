#include <iostream>
#include <string>

int main()
{
    std::cout << "Tapez un entier" << std::endl;
    int i;
    std::cin >> i;
    std::cout << "Vous avez tapé " << i << std::endl;

    std::cout << "Tapez un flottant" << std::endl;
    double d;
    std::cin >> d;
    std::cout << "Vous avez tapé " << d << std::endl;

    std::cout << "Tapez deux entier" << std::endl;
    int j, k;
    std::cin >> j >> k;
    std::cout << "Vous avez tapé " << j << " et " << k << std::endl;    

    std::cout << "Tapez une chaîne de caractères" << std::endl;
    std::string s;
    std::cin >> s;
    std::cout << "Vous avez tapé " << s << std::endl;    
}