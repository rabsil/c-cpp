#include <iostream>
#include <cmath>
//#include <ctime>
#include <chrono>

using namespace std;

constexpr double PI = atan(1) * 4;//ok : atan is constexpr

constexpr int factorial(unsigned n) //C++11 : recursion
{
    return n <= 1 ? 1 : n * factorial(n - 1);
}

constexpr long long int test(long long int n) //c++14
{
	int i = n;
	while(i >= 1)
		i--;
	return i;
}

template<int n>
struct constN //compile time outputs
{
    constN() { cout << n << endl; }
};

int main()
{	    
    auto start = std::chrono::system_clock::now();        
    
	cout << "10! = ";
	constN<factorial(10)> out1;

	cout << "PI = " << PI << endl;

	cout << "test(9999999)=";
	constN<test(2000000)> out3;
	cout << test(2000000) << endl;    
    
    auto end = std::chrono::system_clock::now();
    std::chrono::duration<double> diff = end - start;
    cout << diff.count() << " s taken" << endl;
    
}
