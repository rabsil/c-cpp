#include <iostream>

int main()
{
    int i1 = 3;
    int i2 = 2.5;
    double d1 = i1;
    double d2 = 2.5f;
    unsigned u1 = i1;
    unsigned u2 = -1;

    std::cout << i1 << " " << i2 << std::endl;
    std::cout << d1 << " " << d2 << std::endl;
    std::cout << u1 << " " << u2 << std::endl;
    
    std::cout << "Sizeof int | float -- " 
              << sizeof(int) << " " << sizeof(double) << std::endl;
    
    std::cout << "Sizeof 2 * 3L + 5.0 : " << sizeof(2 * 3L + 5.0) << std::endl;    
    
    int c1 = d2;
    int c2 = static_cast<int>(d2);
    int c3 = (int) d2;
    std::cout << c1 << std::endl;
    std::cout << c2 << std::endl;
    std::cout << c3 << std::endl;  
    
    int i = 2;
    int * pt = &i;
    //long int j = pt;
    //long int j = static_cast<long int>(pt);
    long int k = (int)pt; //ok if -fpermissive
}
    