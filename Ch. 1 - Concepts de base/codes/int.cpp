#include <iostream>
#include <limits>
#include <bit>

int main()
{
	unsigned i = -1;
	if(i == std::numeric_limits<unsigned>::max())
		std::cout << "Two complement" << std::endl;
	else
		std::cout << "Unknown negative representation, or undefined behaviour" << std::endl;
		
    //ugly
	unsigned j = 1;
	char* ptr = (char*)&j;    
	if(*ptr == 1 && *ptr+48 == '1') // (48 is the ascii code of '0')
		std::cout << "Little endian" << std::endl;
	else
		std::cout << "Big endian" << std::endl;
    
    //better in c++20
    if(std::endian::native == std::endian::little)
        std::cout << "Little endian" << std::endl;
    else if (std::endian::native == std::endian::big)
        std::cout << "Big endian" << std::endl;
    else
        std::cout << "Unknown endianess" << std::endl;
    
    for(unsigned i = 5; i >= 0; i--)
        std::cout << i << " ";
    std::cout << std::endl;
}
