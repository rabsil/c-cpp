#include <iostream>
#include <iomanip>
#include <limits>
#include <cmath>

const long double PI = atan(1) * 4;

int main()
{
	 std::cout << left << std::setw(16)
	           << "default pi" << " : " << PI << std::endl
	           << left << std::setw(16) 
	           << "10-digits pi" << " : " << std::setprecision(10) << PI << std::endl
	           << left << std::setw(16)
               << "max-digits pi" << " : " << std::setprecision(std::numeric_limits<long double>::digits10 + 1)
                  << PI << endl;

	std::cout << std::endl;
	
	int n = 42;
	std::cout << "42 - base 8  : "<< std::oct << n << std::endl;
	std::cout << "42 - base 10 : "<< std::dec << n << std::endl;
	std::cout << "42 - base 16 : "<< std::hex << n << std::endl;
}
