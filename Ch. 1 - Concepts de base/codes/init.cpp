#include <iostream>
#include <vector>

using namespace std;

struct A  //class
{
    A(int i) {} //no def cstr   
};

int main()
{
    int i;     //indeterminate
    int j = 2;
    int k {2}; 
    int l = {2};
    
    //int m {2.1}; 
    //int n = {2.1};        
    
    int * p; //undeterminate address
    *p = 3; //seg fault
    
    //int * u = 2; //error
    //int * u = static_cast<int*>(2); //error   
    int * u = (int*)2; //up to no good    
    *u = 3; //seg fault
    
    //const int ci;
    //int & ri;
    //A a;    
}
