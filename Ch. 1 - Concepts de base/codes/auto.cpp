#include <iostream>
#include <cmath>

auto f(int i)
{
    switch(i)
    {
        case 1 : return std::sqrt(i);
        case 2 : return std::cos(i);
        default : return i + 0.;
    }    
}

/*
auto g(int i)
{
    return i > 0 ? "Hello" : 42; //error
}

auto h(int i) //error
{
    if(i > 0)
        return "Hello";
    else
        return 0;
}

auto conv(int i) //error
{
    if(i > 0)
        return cos(i);
    else
        return i;
}
*/

int main()
{
    auto a = 3 + 4;
    std::cout << a << " of type " << typeid(a).name() << std::endl;

    decltype(a) b; 
    b = 7.2;
    std::cout << b << " of type " << typeid(b).name() << std::endl;

    for(int i = 0; i <=3; i++)
        std::cout << f(i) << " of type " << typeid(f(i)).name() << std::endl;

    auto l = {1, 2};
    std::cout << "Type of l : " << typeid(l).name() << std::endl;
}