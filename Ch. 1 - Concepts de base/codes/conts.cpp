#include <iostream>
#include <vector>
#include <array>
#include <list>

int main()
{
    std::array<int,5> a = {1,2,3,4,5}; //constexpr size
    for(unsigned i = 0; i < a.size(); i++)
        std::cout << a[i] << " ";
    std::cout << std::endl;
    
    std::vector<int> v = {1, 2, 3, 4};
    v.push_back(5);    
    v.erase(v.begin() + 2);
    for(int i : v)
        std::cout << i << " ";
    std::cout << std::endl;    
    
    std::list<int> l = {1, 2, 3, 4, 5};
    auto index = l.begin();
    std::advance(index, 2); //with vector, a + just works
    l.erase(index);
    l.push_front(0);
    l.pop_back();
    for(auto it = l.begin(); it != l.end(); it++)
        std::cout << *it << " ";
    std::cout << std::endl;   
}
