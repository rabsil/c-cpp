#include <iostream>

using namespace std;

struct resource
{
    bool locked;
    
    resource() : locked(false) {}    

    bool acquire() 
    {
        if(locked)
            return false;
        else
        {
            locked = true;

            cout << "resource " << this << " locked" << endl;

            return true;
        }    
    }
    bool liberate() 
    {
        if(! locked)
            return false;
        else
        {
            locked = false;

            cout << "resource " << this << " liberated" << endl;

            return true;            
        }        
    }
};

resource r1;
resource r2;

void stuff(int i)
{
    if(i % 2 == 0)
        throw i;
}

void f()
{
    r1.acquire();
    r2.acquire();
    
    try
    {
        stuff(0);
    }
    catch(...)
    {
        r2.liberate();
        r1.liberate();

        throw; // rethrow
    }
        
    r2.liberate();
    r1.liberate();    
}

int main()
{
    f();
}
