#include <iostream>
#include <stdexcept>
#include <string>

struct MyException : std::logic_error
{
    inline MyException(const std::string& s = "") : std::logic_error(s) {}
};

int main()
{
    try
    {
        throw MyException("Hello");   
    }
    catch(const MyException& e)
    {
        std::cout << e.what() << std::endl;   
    }
}
