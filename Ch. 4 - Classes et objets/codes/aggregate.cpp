#include <iostream>

using namespace std;

struct A
{
    int x; double y;
    //A() { cout << "+A()" << endl; } //not an aggregate   
    void print() { cout << x << " " << y; }
};

struct B
{
    int z;
    
    A a;
    
    void print() 
    { 
        cout << z << " {";
        a.print();
        cout << " }";
    }
};

void f()
{
    A a {2, 3};
    a.print();
}

void g()
{
    A a;
    a.print();
}

int main()
{
    f();    
    g();    
    
    A a1;
    a1.print(); cout << endl;
    A a2{}; 
    a2.print(); cout << endl;
    A a3{2};
    a3.print(); cout << endl;
    A a4{2, 1.5};
    a4.print(); cout << endl; cout << endl;
        
    B b1;
    b1.print(); cout << endl;
    B b2{};
    b2.print(); cout << endl;
    B b3{3};
    b3.print(); cout << endl;
    B b4{4,{}};
    b4.print(); cout << endl;
    B b5{4,{1}};
    b5.print(); cout << endl;
    B b6{4,{1,1.5}};
    b6.print(); cout << endl;
}
