#ifndef POINT_HEADER
#define POINT_HEADER

class point
{
    double x, y;

    public:
        point(int x, int y);
        double getX();
        double getY();
        double dist(const point& p);
};

#endif