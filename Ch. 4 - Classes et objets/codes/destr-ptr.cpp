#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

struct A
{
    ~A()
    {
        cout << "-A " << this << endl;   
    }
};

int main()
{
    A a;
    A* pta = &a;
    cout << pta << endl;
    
    vector<A> v = {{},{},{}};
    for_each(v.begin(), v.end(), [](const A & a){cout << &a << endl; });
    
    vector<A*> v2 = {&(v[0]), &(v[1]), &(v[2])};
}
