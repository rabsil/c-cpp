#include <iostream>

using namespace std;

class point
{
    double _x, _y;

    public:
        point(double x = 0, double y = 0) : _x(x), _y(y) {}

        double x() const { return _x; }
        double y() const { return _y; }
    
        string to_string() const
        {
            string s = "(";
            s += p._x;
            s += " , ";
            s += p._y;
            s += ")";
            return s;
        }
};

ostream& operator << (ostream& out, const point& p)
{
    out << p.to_string();
    return out;
}

int main()
{
    point p;
    cout << p << endl; 
    p = point(2,3);
    cout << p << endl;
}
