#include <iostream>
#include <optional>
#include <iomanip>
#include <functional>
#include <compare>

using namespace std;

class NanInt
{
    optional<int> i;
    
    public:
        NanInt() : i(nullopt) 
        {}
    
        NanInt(int i) : i(i) {}
    
        int& value() 
        { 
            return i.value(); 
        }
    
        const int& value() const 
        { 
            return i.value(); 
        }
    
        bool is_NaN() const
        {
            return ! i.has_value();
        }
    
        bool operator==(const NanInt& i)
        {
            if(! is_NaN() && ! i.is_NaN())
                return value() == i.value();
            return false;
        }
    
        partial_ordering operator<=>(const NanInt& i)
        {
            if( ! is_NaN() && ! i.is_NaN())
                return value() <=> i.value();
            return partial_ordering::unordered;
        }
};

template<class Function>
void test_prints(Function&& f, const string& op)
{//since no code is generated, I can't pass the operators as function arguments...
    NanInt nan;
    NanInt i1 = 1;
    NanInt i2 = 2;
    
    cout << "Nan " << op << " Nan ?    " << f(nan, nan) << endl;
    cout << "Nan " << op << " i1  ?    " << f(nan, i1) << endl;
    cout << "i1  " << op << " Nan ?    " << f(i1, nan) << endl;
    cout << "Nan " << op << " 1   ?    " << f(nan, 1) << endl;
    cout << "1   " << op << " Nan ?    " << f(1, nan) << endl;
    cout << "i1  " << op << " i1  ?    " << f(i1, i1) << endl;
    cout << "i1  " << op << " 1   ?    " << f(i1, 1) << endl;
    cout << "1   " << op << " i1  ?    " << f(1, i1) << endl;
    cout << "i1  " << op << " i2  ?    " << f(i1, i2) << endl << endl;
}

int main()
{
    NanInt nan;
    NanInt i1 = 1;
    NanInt i2 = 2;
    cout << boolalpha;
    
    cout << (nan == nan) << endl;
    cout << (nan == i1) << endl;
    cout << (i1 == nan) << endl;
    cout << (nan == 1) << endl;
    cout << (1 == nan) << endl;
    cout << (i1 == i1) << endl;
    cout << (i1 == 1) << endl;
    cout << (1 == i1) << endl;
    cout << (i1 == i2) << endl << endl;
    
    cout << (nan != nan) << endl;
    cout << (nan != i1) << endl;
    cout << (i1 != nan) << endl;
    cout << (nan != 1) << endl;
    cout << (1 != nan) << endl;
    cout << (i1 != i1) << endl;
    cout << (i1 != 1) << endl;
    cout << (1 != i1) << endl;
    cout << (i1 != i2) << endl << endl;
    
    cout << (nan < nan) << endl;
    cout << (nan < i1) << endl;
    cout << (i1 < nan) << endl;
    cout << (nan < 1) << endl;
    cout << (1 < nan) << endl;
    cout << (i1 < i1) << endl;
    cout << (i1 < 1) << endl;
    cout << (1 < i1) << endl;
    cout << (i1 < i2) << endl << endl;
    
    cout << (nan <= nan) << endl;
    cout << (nan <= i1) << endl;
    cout << (i1 <= nan) << endl;
    cout << (nan <= 1) << endl;
    cout << (1 <= nan) << endl;
    cout << (i1 <= i1) << endl;
    cout << (i1 <= 1) << endl;
    cout << (1 <= i1) << endl;
    cout << (i1 <= i2) << endl << endl;
    
    cout << (nan > nan) << endl;
    cout << (nan > i1) << endl;
    cout << (i1 > nan) << endl;
    cout << (nan > 1) << endl;
    cout << (1 > nan) << endl;
    cout << (i1 > i1) << endl;
    cout << (i1 > 1) << endl;
    cout << (1 > i1) << endl;
    cout << (i1 > i2) << endl << endl;
    
    cout << (nan >= nan) << endl;
    cout << (nan >= i1) << endl;
    cout << (i1 >= nan) << endl;
    cout << (nan >= 1) << endl;
    cout << (1 >= nan) << endl;
    cout << (i1 >= i1) << endl;
    cout << (i1 >= 1) << endl;
    cout << (1 >= i1) << endl;
    cout << (i1 >= i2) << endl << endl;
}
