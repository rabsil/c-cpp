#include <iostream>

using namespace std;

struct A
{
    int i;
};

struct B
{
    int i;
    A a;
    
    auto operator<=>(const B& b) const = default;
};

int main()
{
    B b;
    
    cout << (b < b) << endl;
}
