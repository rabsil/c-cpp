#include <iostream>

template<class T>
concept Integral = is_integral_v<T>;

template<class T>
concept SignedIntegral = Integral<T> && is_signed_v<T>; //ok
//SignedIntegral is a total function, defined for any type

//template<Integral T>
//concept SignedIntegral2 = is_signed_v<T>; //ko
//SigneIntegral2 is a function only defined on Integral

//SignedIntegral<int32_t> is true
//SignedIntegral<uint32_t> is false
//SignedIntegral<string> is false

//SignedIntegral2<int32_t> is true
//SignedIntegral2<uint32_t> is false
//SignedIntegral2<string> is undefined

int main()
{    
    
}
