#include "array-spec.hpp"
#include <iostream>

using namespace std;

int main()
{    
    Array<int,5> a1;        
    a1.print();
        
    Array<int,6> a2 = {1,2,3,4,5,6};           
    a2[0] = 42;
    a2.print();
            
    Array<int*,3> a_add;
    int n[3];
    for(unsigned i = 0; i < 3; i++)
    {
        n[i] = i + 1;
        a_add[i] = &(n[i]);
    }
    a_add.print();
    //Array<int*,3> tmp = {&(n[0]), &(n[1]), &(n[2])};
    //tmp.print();
    for(unsigned i = 0; i < 3; i++)
        cout << *(a_add[i]) << " ";
    cout << endl << endl;    
        
    Array<Array<int,2>,3> a2d = {{1,2},{3,4},{5,6}};
    a2d.print();
    for(unsigned i = 0; i < 3; i++)
    {
        for(unsigned j = 0; j < 2; j++)
            cout << a2d[i][j] << " ";
        cout << endl;
    }    
}
