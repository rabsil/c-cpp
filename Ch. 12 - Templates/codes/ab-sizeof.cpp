#include <iostream>

using namespace std;

struct A 
{
    void a() const { cout << "A" << endl; }
};

struct B
{
    int i;
    
    void b() const { cout << "B" << endl; }
};

template<class T>
bool is_A(const T& t)
{
    return sizeof(T) == sizeof(A);   
}

template<class T>
void f(const T& t)
{
   if(is_A(t))
       t.a();
    else
        t.b();
};

int main()
{
    A a;
    f(a);
    
    B b;
    f(b);
}
