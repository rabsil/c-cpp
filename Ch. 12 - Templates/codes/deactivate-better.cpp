#include <iostream>

using namespace std;

template<class T> requires (!std::integral<T>)
struct A
{
    A(T t) {}        
    A(int i) {}
};

int main()
{
    A<double> a(2.0); //ok
    A<int> b(2); //ko
}
