#include <iostream>
#include <stdexcept>
#include <iostream>
#include <initializer_list>
#include <iterator>
#include <algorithm>

template<class T, int N> class Array
{
    T t[N];       
    
    public:
        Array() = default;
    
        Array(const std::initializer_list<T>& l) : Array()
        {
            if(N != l.size())
                throw std::out_of_range("Non matching sizes");
            std::copy(l.begin(), l.end(), t);
        }

        T& operator[](unsigned i) { return t[i]; } 
        const T& operator[](unsigned i) const { return t[i]; }
    
        T& at(unsigned i) 
        {
            range_check(i);
            return (*this)[i];
        }
    
        const T& at(unsigned i) const
        {
            range_check(i);
            return (*this)[i];
        }

        constexpr unsigned size() const noexcept { return N; }                           
        constexpr bool empty() const noexcept { return N == 0; }            
    
        auto begin() noexcept { return std::begin(t); }
        const auto begin() const noexcept { return std::begin(t); }
        auto end() noexcept { return std::end(t); }
        const auto end() const noexcept { return std::end(t); }
    
    private:
        void range_check(unsigned i) const
        {
            if(i >= N)
                throw std::out_of_range("Array access out of bounds");
        }
};

template<class T, int N>
std::ostream& operator<<(std::ostream& out, const Array<T,N>& a)
{
    out << "{ ";
    std::for_each(a.begin(), a.end(), [](const int& i) { std::cout << i << " "; });
    out << "}";
    return out;
}