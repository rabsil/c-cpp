//#include "array-spec.hpp"
#include "array.hpp"
#include <iostream>

using namespace std;

int main()
{    
    Array<int,5> a1;        
    cout << a1 << endl << endl;    
        
    Array<int,6> a2 = {1,2,3,4,5,6};           
    a2[0] = 42;
    
    try
    {
        a2.at(6) = -1;
    }
    catch(...)
    {
        cout << "Couldn't access there" << endl;   
    }
    cout << a2 << endl;
}
