#include <iostream>
#include <concepts>

using namespace std;

template<class T>
concept Duck = requires(T t)
{
    { t.quack() };  
};

template<class T> //well defined
concept FluffyDuck = Duck<T> && requires(T t)
{
    { t.fluff() };
};

/**
template<Duck T> //wrongly defined
concept FluffyDuck = requires(T t)
{
    { t.fluff() };
};
*/

struct Goose
{
    void quack() { cout << "Quack!" << endl; }  
};

struct Duckling
{
    void quack() { cout << "Quick!" << endl; }  
    void fluff() { cout << "Fluff!" << endl; }  
};

struct Cat
{
    void meow() { cout << "Meow!" << endl; }  
};

template<Duck D>
void squish(D d)
{
    d.quack();   
}

template<FluffyDuck D>
void scare(D d)
{
    d.fluff();
    d.quack();    
}

int main()
{
    Goose g; 
    squish(g);
    // scare(g);
    cout << endl;
    
    Duckling d; 
    squish(d); 
    scare(d); 
    cout << endl;
    
    Cat c; 
    //squish(c); 
    //scare(c); 
    cout << endl;
}
