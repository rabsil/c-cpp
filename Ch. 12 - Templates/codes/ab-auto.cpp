#include <iostream>

using namespace std;

struct A 
{ 
    A a() { return A(); } 
};

struct AA 
{ 
    AA a() { return AA(); } 
};

struct B 
{ 
    B b() { return B(); } 
};

struct AB : A, B
{
    AB a() { return AB(); }  
    AB b() { return AB(); }
};

template <class T> 
class Wrapper
{
    T& t;
    public :
        Wrapper (T& t) : t(t) {}
        auto a() { return t.a(); }
        auto b() { return t.b(); }
};

int main()
{
    int i = 2;    
    Wrapper<int> w(i);
    //w.a(); w.b(); 
    
    A a; Wrapper<A> wa(a);
    wa.a(); //wa.b(); 
    
    B b; Wrapper<B> wb(b);
    /*wb.a(); */ wb.b();
        
    AB ab; Wrapper<AB> wab(ab);
    wab.a(); wab.b();
    
    AA aa; Wrapper<AA> waa(aa);
    waa.a(); //waa.b();
}
