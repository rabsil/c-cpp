#include <iostream>

using namespace std;

struct A 
{ 
    A a() { return A(); } 
};

struct AA 
{ 
    AA a() { return AA(); } 
};

struct B 
{ 
    B b() { return B(); } 
};

struct AB : A, B
{
    AB a() { return AB(); }  
    AB b() { return AB(); }
};

template<class T>
concept hasA = requires(T t)
{
    { t.a() };
};

template<class T>
concept hasB = requires(T t)
{
    { t.b() };
};

template<class T>
concept aANDb = hasA<T> && hasB<T>;

template<hasA T>
T fwd(T& t)
{        
    return t.a();
}

template<hasB T>
T fwd(T& t)
{        
    return t.b();
}

template<aANDb T>
T fwd(T& t)
{        
    return T();
}

int main()
{
    A a; B b; AB ab;
    fwd(a);
    fwd(b);
    fwd(ab);
}
