#include <iostream>

using namespace std;

struct A
{
    virtual void f(int) {} //virtual mandatory
};

struct B : A
{
    void f(int) override {}
};

struct C : A
{
    void f(double) override {} //ko
};

int main()
{
    A a;
    B b;
    C c;
}
