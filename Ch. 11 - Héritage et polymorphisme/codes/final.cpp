#include <iostream>

using namespace std;

struct A final 
{};

struct B : A //error
{};

int main()
{
   B b; 
}
