#include <iostream>

using namespace std;

class point
{
    protected:
        int _x, _y;
    
    public:
        point(int a = 0, int b = 0) : _x(a), _y(b) {}

        point & operator =(const point& p)
        {            
            _x = p._x;
            _y = p._y;
            cout << "= point" << endl;            
            return *this;
        }
    
        int x() const 
        { 
            return _x; 
        }
    
        int y() const
        {
            return _y;
        }
};

ostream& operator<< (ostream& out, const point& p)
{
    return (out << "( " << p.x() << " , " << p.y() << " )");   
}

class pointcol : public point
{
    short _r, _g, _b;
    
    public:
        pointcol(int x = 0, int y = 0, int r = 0, int g = 0, int b = 0) : point(x,y), _r(r), _g(g), _b(b) {}
        
        short r() const 
        { 
            return _r; 
        }
        
        short g() const 
        {
            return _g; 
        }
        
        short b() const 
        { 
            return _b; 
        }
};

ostream& operator << (ostream& out, const pointcol& p)
{
    return (out << static_cast<const point&>(p) << " - color " << p.r() << " " << p.g() << " " << p.b());
}

int main()
{
    pointcol p1(1,2,255, 128, 128);
    pointcol p2(4,5,255, 128, 128);

    cout << p1 << endl;
    cout << p2 << endl;
    cout << endl;

    p2 = p1;
    cout << p1 << endl;
    cout << p2 << endl;    

    p2 = pointcol(4,5,255, 128, 128);
    pointcol * pt1 = &p1;
    pointcol * pt2 = &p2;

    pt1 = pt2;    
    cout << *pt1 << endl;
    cout << *pt2 << endl;
}
