#include <iostream>

using namespace std;

class A
{
    public:
        virtual A & operator = (const A&)
        {
            cout << "=A" << endl;
            return *this;
        }
};

class B : public A
{
    public:               
        virtual B & operator = (const B&) //override //ko
        {
            cout << "=B" << endl;
            return *this;
        }
};

int main()
{
    A a1; A * pa1 = &a1; A a2; A * pa2 = &a2;
    B b1; B * pb1 = &b1; B b2; B * pb2 = &b2;    

    *pa1 = *pa2; //= A
    *pb1 = *pb2; //= B
    *pa1 = *pb1; //= A
}
