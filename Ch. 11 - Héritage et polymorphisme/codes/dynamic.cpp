#include <iostream>

using namespace std;

class point
{
    protected:
        int x, y;
    
    public:
        point(int x = 0, int y = 0) : x(x), y(y) {}

        virtual void print() const
        {
            cout << "( " << x << " , " << y << " )";
        }        
};

ostream& operator<<(ostream& out, const point& p)
{
    out << "<<p  ";
    p.print();
    return out;
}

class pointcol : public point
{
    short r, g, b;
    
    public:
        pointcol(int x = 0, int y = 0, int r = 255, int g = 255, int b = 255) 
            : point(x,y), r(r), g(g), b(b) {}

        void print() const override //is virtual as well
        {        
            point::print();
            cout << " - color " << r << " " << g << " " << b;
        }
};

ostream& operator<<(ostream& out, const pointcol& p)
{
    out << "<<pc ";
    p.print();
    return out;
}

int main()
{    
    point p (1,2);
    pointcol pc (3,4,128,255,255);    
    cout << p << endl;   //perfect match static call
    cout << pc << endl;  //perfect match static call
    p.print(); cout << endl; //perfect match static call
    pc.print(); cout << endl; //perfect match static call
    cout << endl;
    
    p = pc; //pp truncated: p is "really" a point
    //pc = p; // ko
    //pc = static_cast<pointcol>(p); //ko    
    cout << p << endl;   //perfect match static call
    cout << pc << endl;  //perfect match static call
    p.print(); cout << endl;  //perfect match static call
    pc.print(); cout << endl; //perfect match static call
    cout << endl;

    p  = point(1,2); point& rp = p;
    pc = pointcol(3,4,128,255,255); pointcol& rpc = pc;
    rp = rpc; //no truncation
    pointcol& rpc2 = static_cast<pointcol&>(rp);// ok, but incoherent result for r, g, b    
    point& rp2 = pc;
    cout << rp << endl;   //perfect match static call (rp was initialised as point&)
    cout << rpc << endl;  //perfect match static call
    cout << rpc2 << endl; //perfect match static call (rp was initialised as point&)
    rp.print(); cout << endl;   //polymorph call: (rp was initialised as point&)
    rpc.print(); cout << endl;  //polymorph call: rpc is a pointcol
    rpc2.print(); cout << endl; //polymorph call: (rp was initialised as point&)
    rp2.print(); cout << endl;  //polymorph call: rp2 is a pointcol
    cout << endl;
        
    point& rpp = pc;     //rpp initialised as ref of pointcol
    cout << rpp << endl; //perfect match static call
    rpp.print();         //polymorph call: rpp is a pointcol
    cout << endl << endl;
    
    p = point(1,2); pc = pointcol(3,4,128,255,255);
    point * ptp = &p;
    pointcol * ptpc = &pc;
    ptp = ptpc; //no truncation
    cout << (*ptp) << endl;  //perfect match static call
    cout << (*ptpc) << endl; //perfect match static call
    ptp->print(); cout << endl;  //polymorph call: ptp is a pointcol
    ptpc->print(); cout << endl; //polymorph call: ptp is a pointcol
    cout << endl;
        
    ptp = &p;
    //ptpc = ptp; //ko    
    //ptpc = static_cast<pointcol*>(ptp);//ok, but seg fault    
    cout << (*ptp) << endl;  //perfect match static call
    cout << (*ptpc) << endl; //perfect match static call
    ptp->print();  cout << endl; //polymorph call: ptp is a point
    ptpc->print(); cout << endl; //polymorph call: ptp is a pointcol
    cout << endl;

    if(pointcol * converted = dynamic_cast<pointcol*>(ptp))
    {        
        cout << (*converted) << endl;     //perfect match static call
        converted->print(); cout << endl; //polymorph call: converted is a pointcol
    }
    else
        cout << "You cannot convert this point to a pointcol" << endl;        
}


