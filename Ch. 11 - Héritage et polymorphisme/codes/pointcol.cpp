#include <iostream>

using namespace std;

class point
{
    int _x, _y;
    
    public:
        point(int x = 0, int y = 0) : _x(x), _y(y) {}
        
        void set_location(int x, int y)
        {
            _x = x;
            _y = y;
        }
    
        int x() const { return _x; }
        int y() const { return _y; }
};

ostream& operator <<(ostream& out, const point& p)
{
    return (out << "( " << p.x() << " , " << p.y() << " )");
}    

class pointcol : public point
{
    short _r, _g, _b;
    
    public:
        pointcol(int x, int y, short r, short g, short b) : point(x,y), _r(r), _g(g), _b(b) {}
        
        short r() const { return _r; }
        short g() const { return _g; }
        short b() const { return _b; }
};

ostream& operator <<(ostream& out, const pointcol& p)
{
    return (out << static_cast<const point&>(p) << p.r() << " " << p.g() << " " << p.b() << endl);
}

int main()
{
    pointcol p(1,2,80,0,0);
    cout << p << endl;

    p.set_location(3,4);
    cout << p << endl;    
}
