\input{../common/header.tex}
\input{../common/cmds.tex}

\title{Ch. 2 - Pointeurs et références}

\begin{document}
\input{../common/front.tex}

\section{Introduction}

\begin{frame}
\frametitle{Overview}
\begin{itemize}[<+->]
\item Dans les deux cas, les pointeurs et références sont utilisés comme des étiquettes pour désigner d'autres objets
\item On les utilise
	\begin{itemize}
	\item si l'on veut propager en écriture des effets de bords (modifier un paramètre de fonction)
	\item si on ne veut pas que des copies implicites de données soient effectuées
	\end{itemize}
%\item Les références sont un concept \cpp\ uniquement
%	\begin{itemize}
%	\item En \texttt{C}, il n'existe que les pointeurs
%	\end{itemize}
\item Il y a des cas où l'on ne peut pas se passer de pointeurs / références
\item Toutefois, ces concepts ne sont \emph{pas} les mêmes
	\begin{itemize}
	\item Syntaxe et manipulation différentes
	\item On peut faire des écritures avec des pointeurs invalides avec des références
	\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Idée}
\begin{itemize}[<+->]
\item Pointeur = adresse d'un objet
\item Référence = alias d'un objet
\end{itemize}
\begin{exampleblock}<+->{Métaphore}
	\begin{itemize}[<+->]
	\item Constructeur d'ordinateurs : « Banana »
		\begin{itemize}
		\item Variable, difficile à copier
		\end{itemize}
	\item Carte de visite de « Banana » avec coordonnées postales
		\begin{itemize}
		\item Pointeur, facile à copier, à changer les coordonnées
		\end{itemize}
	\item Surnom du constructeur : « les iDiots »
		\begin{itemize}
		\item Référence : alias vers un constructeur existant
		\item Ne peut être associé si « Banana » n'existe pas
		\end{itemize}
	\end{itemize}
\end{exampleblock}
\end{frame}

\section{Pointeurs}

\begin{frame}
\frametitle{Concept de pointeur}
\begin{itemize}[<+->]
\item Un pointeur vers un type \texttt{T} contient l'adresse d'un élément de type \texttt{T}
\item On peut construire un pointeur en
	\begin{itemize}
	\item l'affectant à un autre pointeur : \lstinline|int * pt2 = pt1;|
	\item prenant l'adresse d'une variable : \lstinline|int * pt = &i;|
	\end{itemize}
\item On accède au contenu d'un pointeur avec \texttt{*}
	\begin{itemize}
	\item On dit qu'on déférence le pointeur : \lstinline|int j = *pt;|
	\end{itemize}
\item Un pointeur a une adresse, et est systématiquement de la taille du bus d'adresse
	\begin{itemize}
	\item En 32 bits, \lstinline|sizeof(T*)| est égal à $4$
	\item En 64 bits, \lstinline|sizeof(T*)| est égal à $8$
	\end{itemize}
\item On peut créer des \lstinline|void*|
	\begin{itemize}
	\item Pointeur vers un type \lstinline|void|, incomplet
	\item On ne peut pas déférencer un \lstinline|void*|
	\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Pointeur nuls}
\begin{itemize}[<+->]
\item On peut créer des pointeurs nuls avec \lstinline|nullptr|
%	\begin{itemize}
%	\item En \texttt{C} : \lstinline|NULL|
%	\item En \cpp\ : \lstinline|nullptr|
%	\end{itemize}
\item \lstinline|NULL| est une macro : \lstinline|\#define NULL 0|
    \begin{itemize}
    \item Relique du \texttt{C}
    \end{itemize}
\item \lstinline|nullptr| est un immédiat
\item Déférencer un pointeur nul a un comportement indéterminé
\item Créer un pointeur n'alloue pas de mémoire vers la zone pointée
	\begin{itemize}
	\item \lstinline|int * p;| n'alloue pas \lstinline|sizeof(int)| bytes en mémoire pour \texttt{*p}
    \item \lstinline|int * p; *p = 2;| va probablement provoquer une erreur de segmentation
	\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Conversions}
\begin{itemize}[<+->]
\item Conversions implicites de \texttt{T*} vers \lstinline|void*|
\item Aucune autre conversion implicite entre pointeurs n'est possible
%\item Pas de conversion de \lstinline|int| vers \texttt{T*}, «~sauf~» avec \lstinline|NULL|
\end{itemize}
\begin{block}<+->{Hygiène de programmation}
	\begin{itemize}[<+->]
	\item N'utilisez jamais \lstinline|NULL| en \cpp
		\begin{itemize}
		\item \lstinline|nullptr| ne permet pas de conversions implicites
		\end{itemize}
	\end{itemize}
\end{block}
\end{frame}

\begin{frame}[containsverbatim]
\frametitle{Exemple}
\begin{itemize}
\item Fichier \texttt{ptr.cpp}
\end{itemize}
\begin{lstlisting}
int main()
{
    int i = 3;
    int * pti = &i;

    cout << "i = " << i << ", pti (" << pti << ") : " << *pti << endl;
    cout << "Pointer address : " << &pti << " of size " << sizeof(pti) << endl;

    i++;
    cout << "i = " << i << ", pti (" << pti << ") : " << *pti << endl;
    cout << "Pointer address : " << &pti << " of size " << sizeof(pti) << endl;

    double d = 2.5;
    double * ptd = &d;
    //pti = ptd; //Error
    *pti = *ptd; //ok

    int * ptn = NULL; //ok
    ptn = nullptr; //ok
    //cout << *ptn << endl; //undefined behaviour

    void * ptv = pti;
    //cout << *ptv << endl; //seg fault
}
\end{lstlisting}
\end{frame}

\begin{frame}
\frametitle{Syntaxe pointeurs et constantes}
\begin{exampleblock}<+->{Pointeur constant de \texttt{double}}
	\begin{itemize}[<+->]
	\item \lstinline|double d = 2;|
	\item \lstinline|double * const pt = &d;|
	\end{itemize}
\end{exampleblock}
\begin{exampleblock}<+->{Pointeur de \texttt{double} constant}
	\begin{itemize}[<+->]
	\item \lstinline|const double d = 2;|
	\item \lstinline|const double * pt = &d;|
	\end{itemize}
\end{exampleblock}
\begin{exampleblock}<+->{Pointeur constant de \texttt{double} constant}
	\begin{itemize}[<+->]
	\item \lstinline|const double d = 2;|
	\item \lstinline|const double * const pt = &d;|
	\end{itemize}
\end{exampleblock}
\end{frame}

\begin{frame}[containsverbatim]
\frametitle{Illustration}
\begin{itemize}
\item Fichier \texttt{ptr-cst.cpp}
\end{itemize}
\begin{lstlisting}
int main()
{
    int i = 2;
    const int ci = 3; //int const ci = 3 similaire
    i += 2;
    //ci += 2; //ko

    int * pi = &i;
    cout << pi << " : " << i << endl;
    *pi = 3;
    cout << pi << " : " << i << endl;

    int * const cpi = &i; //ptr constant
    *cpi = 5;
    //cpi++;

    const int * pic = &ci; //ptr d'entier constant
    //*pic = 4;
    pic++;

    const int * const cpic = &ci; //ptr cst d'entier cst
    //*cpic = 4;
    //cpic++;
}
\end{lstlisting}
\end{frame}

\begin{frame}
\frametitle{Arithmétique}
\begin{itemize}[<+->]
\item On peut « déplacer » un pointeur
	\begin{itemize}
	\item C'est une adresse : on se déplace en mémoire
	\end{itemize}
\item Si \texttt{pt} est un pointeur de type \texttt{T}, alors \texttt{pt + k} déplace l'adresse de \lstinline|k * sizeof(T)| bytes
	\begin{itemize}
	\item \texttt{pt++} déplace \texttt{pt} de \lstinline|sizeof(T)|
	\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}[containsverbatim]
\frametitle{Illustration}
\begin{itemize}
\item Fichier \texttt{print-str.c}
\end{itemize}
\begin{lstlisting}
void print_str(const char* s)
{
    const char* pt = s;
    while(*pt != '\0')
    {
        cout << *pt;
        pt++;
    }
}

int main()
{
    const char* s = "Hello World!\n";
	print_str(s);
}
\end{lstlisting}
\end{frame}

\begin{frame}[containsverbatim]
\frametitle{Pointeurs vers des temporaires}
\begin{alertblock}{Attention}
	\begin{itemize}
	\item Ne retournez pas de pointeurs vers une variable locale
	\end{itemize}
\end{alertblock}
\begin{lstlisting}
int * f()
{
	int i = 2;
	return &i;
}

int main()
{
	cout << *f() << endl; //undefined behaviour
}
\end{lstlisting}
\end{frame}

\begin{frame}
\frametitle{Conclusion}
\begin{itemize}[<+->]
\item Les pointeurs sont des adresses
\item On peut construire un pointeur par affectation ou par prise d'adresse (\texttt{\&} préfixé)
\item On accède au contenu pointé par déférencement (\texttt{*} préfixé)
    \begin{itemize}
    \item Fonctionne (entre autres) si la mémoire n'est pas désallouée
    \end{itemize}
\item On dispose d'une arithmétique de pointeur pour avancer en mémoire
\item Un pointeur peut être nul
\end{itemize}
\begin{block}<+->{Hygiène de programmation}
	\begin{itemize}[<+->]
	\item N'utilisez \emph{jamais} \lstinline|NULL| en \cpp
    \item Privilégier les références
	\end{itemize}
\end{block}
\end{frame}

\section{Pointeurs et tableaux}

\begin{frame}
\frametitle{Les tableaux «~comme en \texttt{C}~»}
\begin{itemize}[<+->]
\item En \texttt{C}, il n'existe pas de conteneurs standards
\item Utilisation de tableaux «~en dur~», avec des pointeurs
    \begin{itemize}
    \item Toujours possible en \cpp
    \end{itemize}
\item La taille du tableau doit pouvoir être déterminée à la compilation
	\begin{itemize}
	\item Mention explicite
	\end{itemize}
\item Le type \texttt{T[]} est implicitement converti vers \texttt{T*} quand
	\begin{enumerate}
	\item ce n'est pas un opérande de \texttt{sizeof} et \texttt{\&} (prise d'adresse)
	\item ce n'est pas un littéral de chaîne de caractère
	\end{enumerate}
\item Les tableaux «~n'embarquent pas leur taille~»
	\begin{itemize}
	\item Il faut la spécifier quand on en a besoin
%	\item En \cpp, on utilise des conteneurs
	\end{itemize}
\item Les éléments « non spécifiés » sont initialisés à zéro
	\begin{itemize}
	\item Si on spécifie plus d'éléments que la taille explicite : erreur
    \item Si aucun élément spécifié : valeurs indéterminées
	\end{itemize}
\end{itemize}
\begin{block}<+->{Hygiène de programmation}
    \begin{itemize}
    \item Éviter : utiliser les conteneurs
    \end{itemize}
\end{block}
\end{frame}

\begin{frame}[containsverbatim]
\frametitle{Syntaxe : exemple}
\begin{itemize}
\item Fichier \texttt{tab.cpp}
\end{itemize}
\begin{lstlisting}
long unsigned sneaky(int array[])
{
    return sizeof(array) / sizeof(*array);
}

int main()
{
    int t1[5] = {1,2,3,4};
    int t2[] = {1,2,3,4,5};
    int * t3 = t2;
    //int t4[];
    //int t5[5] = {1,2,3,4,5,6};
    int t6[8]; // = {}

    for(int i = 0; i < 5; i++)
        cout << t1[i] << " " << t2[i] << " " << t3[i] << " " << t6[i] << endl;

    cout << (sizeof(t6) / sizeof(*t6)) << endl;
    cout << sneaky(t6) << endl;
}
\end{lstlisting}
\end{frame}

\begin{frame}
\frametitle{Tableau en mémoire}
\begin{itemize}[<+->]
\item Les données d'un tableau sont allouées de manière contiguë en mémoire
\item Même dans un tableau à plusieurs dimensions
\item On peut y accéder à partir de l'adresse du premier élément
	\begin{itemize}
	\item \lstinline|int * t = \{2,3,1,0,9,4,7,8\};|
	\item L'adresse du $4$ (6e élément) est égal à \lstinline|(t + 5)|
	\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Illustration}
\begin{center}
\includegraphics[width=.9\textwidth]{pics/tab.pdf}
\end{center}
\end{frame}

\begin{frame}[containsverbatim]
\frametitle{Exemple}
\begin{itemize}
\item Fichier \texttt{tab.cpp}
\end{itemize}
\begin{lstlisting}
int main()
{
    int t2d[2][3];
    for(unsigned i = 0; i < 2; i++)
        for(unsigned j = 0; j < 3; j++)
            t2d[i][j] = i * j;
    for(unsigned i = 0; i < 2; i++)
    {
        for(unsigned j = 0; j < 3; j++)
            cout << t2d[i][j] << " ";
        cout << endl;
    }
    cout << endl;

    int * it = (int*)t2d; //felony
    for(unsigned i = 0; i < 2; i++)
    {
        for(unsigned j = 0; j < 3; j++)
        {
            cout << *it << " ";
            it++;
        }
        cout << endl;
    }
}
\end{lstlisting}
\end{frame}

\begin{frame}
\frametitle{Conclusion}
\begin{itemize}[<+->]
\item On peut modéliser des tableaux à l'aide de pointeurs
\end{itemize}
\begin{block}<+->{Hygiène de programmation}
	\begin{itemize}[<+->]
	\item Éviter en \cpp
	\end{itemize}
\end{block}
\begin{itemize}[<+->]
\item On accède aux éléments à l'aide de l'opérateur \texttt{[]}, ou à partir de l'adresse du premier élément
\item La taille des tableaux doit être connue à la compilation
\end{itemize}
\end{frame}

\section{Références}

\begin{frame}
\frametitle{Concept de référence}
\begin{itemize}[<+->]
%\item Concept \cpp\ uniquement
\item Alias vers une variable existante
	\begin{itemize}
	\item Ne peut pas être « nul » (doit exister)
	\end{itemize}
\item Se manipule comme une variable
\item Conversions implicites possibles à l'affectation
\end{itemize}
\begin{block}<+->{Hygiène de programmation}
	\begin{itemize}[<+->]
	\item Utilisez les références quand vous pouvez, les pointeurs quand vous devez
	\end{itemize}
\end{block}
\begin{itemize}[<+->]
\item Les références sont constantes
	\begin{itemize}
	\item Réaffecter une référence réaffecte l'objet référencé, pas la référence
	\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}[containsverbatim]
\frametitle{Exemple}
\begin{itemize}
\item Fichier \texttt{ref.cpp}
\end{itemize}
\begin{lstlisting}
int main()
{
	int i = 2;
	int & ri = i;
	//int & rj;
	cout << i << " " << ri << endl;

	i++;
	cout << i << " " << ri << endl;
	ri = 5;
	cout << i << " " << ri << endl;

	int j = 8;
	ri = j;

	cout << i << " " << ri << endl;
}
\end{lstlisting}
\begin{itemize}
\item En pratique, ces références sont des références de \emph{lvalue}
\item Il existe d'autres types de référence (cf. Ch. 3 \& 10)
\end{itemize}
\end{frame}

\section{Les lvalue}

\begin{frame}
\frametitle{Intuition}
\begin{exampleblock}<+->{lvalue}
	\begin{itemize}[<+->]
	\item «~Valeur qui peut apparaître à gauche d'un opérateur d'affectation~»
	\end{itemize}
\end{exampleblock}
\begin{itemize}[<+->]
\item Définition insuffisante
\end{itemize}
\begin{alertblock}<+->{Remarque}
	\begin{itemize}[<+->]
	\item \lstinline|const int a = 2; //ok|
	\item \lstinline|a = 3; //ko|
	\end{itemize}
\end{alertblock}
\end{frame}

\begin{frame}
\frametitle{Notion de lvalue}
\begin{exampleblock}<+->{Intuition}
	\begin{itemize}[<+->]
	\item \lstinline|double x, y, a, b;|
	\item \lstinline|y = a*x + b; //ok|
	\item \lstinline|a*x + b = y; //ko|
	\item \lstinline|(x + 1) = 4; //ko|
	\end{itemize}
\end{exampleblock}
\begin{itemize}[<+->]
\item Le premier opérande \emph{doit} référencer un emplacement mémoire non temporaire
	\begin{itemize}
	\item Autres langages : variable, en \cpp, pas assez précis
%	\item Pas assez précis en \texttt{C} / \cpp
	\end{itemize}
\item Contrainte nécessaire à plusieurs endroits dans le langage % (\texttt{=}, \texttt{++}, références, etc.)
\item Propriété très importante pour les \emph{expressions}
%	\begin{itemize}
%	\item En dépit du nom
%	\end{itemize}
\item Ce qui n'est pas une \texttt{lvalue} est une \emph{rvalue} (immédiats, temporaires, anonymes, etc.)
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Conversions entre lvalues et rvalues}
\begin{itemize}[<+->]
\item Souvent, les opérateurs (et fonctions) requièrent des arguments rvalues
\end{itemize}
\begin{exampleblock}<+->{Exemple}
	\begin{itemize}
	\item \lstinline|int i = 1;|
	\item \lstinline|int j = 2;|
	\item \lstinline|int k = i + j;|
	\end{itemize}
\end{exampleblock}
\begin{enumerate}[<+->]
\item \texttt{i} et \texttt{j} sont des lvalue
\item \texttt{+} requiert des rvalue
\item \texttt{i} et \texttt{j} sont convertis en rvalue
\item Une rvalue est retournée
\end{enumerate}
\end{frame}

\begin{frame}
\frametitle{Règles de conversions}
\begin{exampleblock}<+->{Conversion lvalue vers rvalue}
	\begin{itemize}[<+->]
	\item Toutes les lvalues qui ne sont pas des tableaux, des fonctions et des types incomplets peuvent être convertis en rvalues
	\end{itemize}
\end{exampleblock}
\begin{alertblock}<+->{Conversion rvalue vers lvalue}
	\begin{itemize}[<+->]
	\item Impossible implicitement
	\item Le résultat d'un opérateur (rvalue) peut être explicitement affecté en une lvalue
	\end{itemize}
\end{alertblock}
\begin{itemize}[<+->]
\item On peut produire des lvalue à partir de rvalue explicitement
	\begin{itemize}
	\item Le déférencement prend une rvalue et produit une lvalue
	\item L'opérateur \texttt{\&} prend une lvalue et produit une rvalue
	\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}[containsverbatim]
\frametitle{Exemple}
\begin{itemize}
\item Fichier \texttt{lrvalue-conv.cpp}
\end{itemize}
\begin{lstlisting}
int main()
{
	int a[] = {1, 2};
	int* pt = &a[0];
	*(pt + 1) = 10;   //OK : p + 1 is an rvalue, but *(p + 1) is an lvalue

	//taking adress
	int i = 10;
	//int* pti = &(i + 1);   // KO : lvalue required
	int* pti = &i;           // OK: i is an lvalue
	//&i = 20;               // KO : lvalue required

	//reference making
	//std::string& sref = std::string(); //KO : non const-ref init from rvalue
}
\end{lstlisting}
\end{frame}

\begin{frame}[containsverbatim]
\frametitle{Références de \texttt{lvalue}}
\begin{itemize}
\item Fichier \texttt{lvalue-ref.cpp}
\end{itemize}
\begin{lstlisting}
int n = 5;
int& truc = n;

int& brol() { return n; }

int main() {
	cout << brol() << endl;
	brol() = 10;
	cout << brol() << endl;

	truc = 15;
	cout << brol() << endl;

	//int & i = 2;
}
\end{lstlisting}
\begin{itemize}
\item \texttt{n} est une \texttt{lvalue}, \texttt{brol()} aussi
\item Les références de \texttt{lvalue} sont des \texttt{lvalue}
	\begin{itemize}
	\item Utile pour l'opérateur \texttt{[]}
	\item \texttt{v[10] = 42;}
	\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}[containsverbatim]
\frametitle{La motivation des contraintes}
\begin{itemize}
\item On ne veut pas pouvoir réaffecter un temporaire / immédiat
\item Fichier \texttt{rvalue-conv.cpp}
\end{itemize}
\begin{lstlisting}
int a = 42; int b = 43;

 // a and b are both lvalues:
a = b; // ok
b = a; // ok
a = a * b; // ok

// a * b is an rvalue:
int c = a * b; // ok, rvalue on right hand side of assignment
//a * b = 42; // error, rvalue on left hand side of assignment

//2 is a rvalue
int d = 2;
//int & rd = 2; //error
const int& e = 2; //ok : you are allowed to bind a const lvalue to a rvalue

//conv
int f = 2; double g = 2.42;
//long int & rf = e;  //error
//int& rg = g; //error
const int& rgg = g; //ok : you are allowed to bind a const lvalue to a rvalue
\end{lstlisting}
\begin{itemize}
\item Il existe néanmoins des références de rvalue (Cf. Ch. 9)
\item Le fait qu'une expression soit une rvalue ou une lvalue est appelé la \emph{value category}
\end{itemize}
\end{frame}

\section{Pointeurs de fonctions}

\begin{frame}
\frametitle{Adresse d'une fonction}
\begin{itemize}[<+->]
\item Les fonctions ont des adresses dans le segment \texttt{.text}
\item On peut donc créer des pointeurs de fonction
\item Utile pour passer une fonction en paramètre d'une autre fonction
\end{itemize}
\begin{exampleblock}<+->{Syntaxe}
	\begin{itemize}[<+->]
	\item \texttt{ReturnType (*Name)(parameters)}
	\item \lstinline|void (*my_ptr)(int)| : \texttt{my\_ptr} est un pointeur de fonction prenant en paramètre un \lstinline|int| et retournant un \lstinline|void|
	\end{itemize}
\end{exampleblock}
\begin{itemize}[<+->]
\item Il existe également divers wrappers
\end{itemize}
\end{frame}

\begin{frame}[containsverbatim]
\frametitle{Exemple}
\begin{itemize}
\item Fichier \texttt{fct-ptr.cpp}
\end{itemize}
\begin{lstlisting}
void f(int a)
{
	cout << a << endl;
}

int main()
{
	void (*ptr)(int) = &f;
	//void (*ptr)(int) = f; //similar

	//Function call
	(*ptr)(10);
	//ptr(10);
}
\end{lstlisting}
\begin{itemize}
\item \texttt{f} a le type \lstinline|void (*)(int)|
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Le wrapper \texttt{std::function}}
\begin{itemize}[<+->]
\item \texttt{std::function} est un wrapper de pointeur de fonction
	\begin{itemize}
	\item \texttt{std::function<ReturnType (parameters)> my\_ptr}
	\item \lstinline|std::function<void (int)> ptr| : \texttt{ptr} est une fonction prenant en paramètre un \lstinline|int| et retournant un \lstinline|void|
	\end{itemize}
\item On peut créer des pointeurs de fonction membres (aka méthodes)
	\begin{itemize}
	\item Utilisation de l'opérateur de résolution de portée \texttt{::}
	\end{itemize}
\item Le premier paramètre est \emph{toujours} la classe de l'objet sur lequel on appelle la fonction
	\begin{itemize}
	\item \tiny{\lstinline|std::function<void (Rectangle&, double)> f = &Rectangle::setX|}
	\end{itemize}
\item On peut aussi utiliser les templates de manière transparente
\end{itemize}
\begin{block}<+->{Hygiène de programmation}
    \begin{itemize}[<+->]
    \item Privilégier \texttt{std::function} aux pointeurs de fonctions
    \item Privilégier les templates à \texttt{std::function} (cf. Ch. 13)
    \end{itemize}
\end{block}
\end{frame}

\begin{frame}[containsverbatim]
\frametitle{Fonction indépendante}
\begin{itemize}
\item Fichier \texttt{fct-ptr.cpp}
\end{itemize}
\begin{lstlisting}
int fwd(std::function<int (int, int)> f, int a, int b)
{
	return f(a, b);
}

int add(int a, int b)
{
	return a + b;
}

int main()
{
	cout << fwd(add, 2, 3) << endl;
}
\end{lstlisting}
\end{frame}

\begin{frame}[containsverbatim]
\frametitle{Fonction membre}
\begin{itemize}
\item Fichier \texttt{fct-ptr.cpp}
\end{itemize}
\begin{lstlisting}
struct A
{
	int i;
	A(int i) : i(i) {}
	int add(int j) { return i += j; }
};

int fwd(function<int (A&, int)> member, A& a, int j)
{
	return member(a, j);
}

int main()
{
	A a(2);
	cout << fwd(&A::add, a, 3) << endl;
}
\end{lstlisting}
\end{frame}

\begin{frame}[containsverbatim]
\frametitle{Exemple d'utilisation}
\begin{itemize}
\item Fichier \texttt{foreach.cpp}
\end{itemize}
\begin{lstlisting}
void foreach_ro(const vector<int>& tab, function<void(int)> f) {
    for(int i = 0; i < tab.size(); i++)
        f(tab[i]);
}

void foreach_rw(vector<int>& tab, function<int (int)> f) {
    for(int i = 0; i < tab.size(); i++)
        tab[i] = f(tab[i]);
}

void print(int i) {
    cout << i << " ";
}

int increment(int i) {
    return i + 1;
}

int main()  {
    vector<int> tab = {1,2,3,4,5};
    foreach_ro(tab, print); cout << endl;

    foreach_rw(tab, increment);
    foreach_ro(tab, print); cout << endl;
}
\end{lstlisting}
\end{frame}

\begin{frame}
\frametitle{Remarques}
\begin{itemize}[<+->]
\item On ne peut pas allouer ou désallouer de la mémoire à l'aide d'un pointeur de fonction
	\begin{itemize}
	\item Le segment \texttt{.text} est en lecture seul
	\end{itemize}
\item On peut créer des tableaux de pointeurs de fonction
	\begin{itemize}
	\item \lstinline|void (*my_fct_array[])(int)|
	\end{itemize}
\item Les pointeurs de fonctions permettent d'éviter la redondance de code
	\begin{itemize}
	\item \texttt{qsort}
	\item Comparateurs
	\end{itemize}
\end{itemize}
\end{frame}


\end{document}
