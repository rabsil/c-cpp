#include <iostream>

using namespace std;

struct A {};

A f() 
{
    return A();
}

int main()
{
    int a[] = {1, 2};
    int* pt = &a[0];
    *(&a[0]) = 3; //a[0] = 3;
    *(pt + 1) = 10;   //OK : p + 1 is an rvalue, but *(p + 1) is an lvalue

    //taking adress
    int i = 10;
    //int* pti = &(i + 1);   // KO : lvalue required
    int* pti = &i;           // OK: i is an lvalue
    //&i = 20;               // KO : lvalue required

    //reference making        
    A& aa = f();
}