#include <iostream>
#include <vector>
#include <functional>

using namespace std;

void insertion_sort_increase(vector<int>& v)
{
    for (int i = 1; i < v.size(); i++)
    {
        int x = v[i];
        int j = i - 1;
        while(j >= 0 && v[j] > x)
        {
            v[j + 1] = v[j];
            j--;
        }
        v[j + 1] = x;
    }
}

void insertion_sort_decrease(vector<int>& v)
{
    for (int i = 1; i < v.size(); i++)
    {
        int x = v[i];
        int j = i - 1;
        while(j >= 0 && v[j] < x)
        {
            v[j + 1] = v[j];
            j--;
        }
        v[j + 1] = x;
    }
}

bool higher(int i, int j)
{
    return i > j;
}

bool lower(int i, int j)
{
    return i < j;
}

void insertion_sort_generic(vector<int>& v, function<bool(int,int)> is_higher = higher)
{
    for (int i = 1; i < v.size(); i++)
    {
        int x = v[i];
        int j = i - 1;
        while(j >= 0 && is_higher(v[j], x))
        {
            v[j + 1] = v[j];
            j--;
        }
        v[j + 1] = x;
    }
}

void print(const vector<int>& v)
{
    for(int i = 0; i < v.size(); i++)
        cout << v[i] << " ";
    cout << endl;
}

int main()
{
    vector<int> v = {2, 4, 5, 3, 1};
    insertion_sort_increase(v);
    print(v);    
    
    insertion_sort_decrease(v);
    print(v);
    
    insertion_sort_generic(v, higher);
    print(v);
    
    insertion_sort_generic(v, lower);
    print(v);
    
    insertion_sort_generic(v);
    print(v);
}
