#include <iostream>
#include <vector>
#include <functional>

using namespace std;

void foreach_ro(const vector<int>& tab, function<void(int)> f)
{
    for(int i = 0; i < tab.size(); i++)
        f(tab[i]);
}

void foreach_rw(vector<int>& tab, function<int (int)> f)
{
    for(int i = 0; i < tab.size(); i++)
        tab[i] = f(tab[i]);
}

void print(int i)
{
    cout << i << " ";
}

int increment(int i)
{
    return i + 1;
}

int main()  
{
    vector<int> tab = {1,2,3,4,5};    

    foreach_ro(tab, print);
    cout << endl;
    
    foreach_rw(tab, increment);
    foreach_ro(tab, print);
    cout << endl;
}
