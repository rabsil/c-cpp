#include <iostream>

using namespace std;

void print_str(const char* s)
{
    const char* pt = s;
    while(*pt != '\0')
    {
        cout << *pt;
        pt++;
    }
}

int main()
{
    const char* s = "Hello World!\n";
	print_str(s);
}
