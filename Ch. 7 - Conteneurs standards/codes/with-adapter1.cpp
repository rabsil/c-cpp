#include <algorithm>
#include <vector>
#include <iostream>
#include <ranges>

using namespace std;

int main() 
{
    const vector numbers = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

    auto even = [](int i) { return 0 == i % 2; };
 
    for(auto& i : numbers | views::filter(even) | views::drop(1) | views::reverse) 
        cout << i << ' ';
    cout << endl;
}

//source: https://www.cppstories.com/2022/ranges-composition/