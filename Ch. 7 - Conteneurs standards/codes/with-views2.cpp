#include <string>
#include <iomanip>
#include <iostream>
#include <ranges>

using namespace std;

int main() 
{
    const string txt { "    Hello World" };
    cout << txt << endl;

    auto conv = ranges::transform_view { 
        ranges::drop_while_view{txt, ::isspace}, 
        ::toupper 
    };

    string tmp(conv.begin(), conv.end());
    cout << tmp << endl;
}

//source: https://www.cppstories.com/2022/ranges-composition/